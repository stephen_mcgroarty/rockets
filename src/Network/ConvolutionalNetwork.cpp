#include "ConvolutionalNetwork.h"
#include <assert.h> 
#include <stdlib.h> 
#include <string>
#include <sstream>
#include <cmath>


ConvolutionalNetwork::ConvolutionalNetwork(int numberOfFilters, 
										   int filterSize,
										   Network* previousLayer):
	Network(previousLayer->GetOutputWidth(),previousLayer->GetOutputHeight(),numberOfFilters),
	m_FilterSize(filterSize),
	m_InputDepth(previousLayer->GetOutputDepth()),
	m_InputWidth(previousLayer->GetOutputWidth()),
	m_InputHeight(previousLayer->GetOutputHeight())
{
	// Size of the filter
	int size = m_FilterSize*m_FilterSize*m_InputDepth;

	m_InputVolume = previousLayer->GetOutputWidth()*previousLayer->GetOutputHeight()*previousLayer->GetOutputDepth();

	m_pGradients = std::shared_ptr<float> (new float[m_InputVolume]);

	// Initalize the filters
	for (int filterNumber = 0; filterNumber < numberOfFilters; ++filterNumber)
	{

		m_Filters.push_back( std::unique_ptr<float>(new float[size]) );

		// Create buffer for the gradients fo the neurons.
		m_GradientSum.push_back( std::unique_ptr<float>( new float[size] ) );
		memset(m_GradientSum.back().get(),0.0f,sizeof(float)*size);


		// Randomize the weights.
		for (int index = 0; index < size; ++index)
		{
			m_Filters.back().get()[index] = static_cast<float>(rand())/static_cast<float>(RAND_MAX);
		}
	}

	// Initalize the Biases
	m_pBiases = std::unique_ptr<float>(new float[numberOfFilters]);
	// Randomize the weights.
	for (int index = 0; index < numberOfFilters; ++index)
	{
		m_pBiases.get()[index] = static_cast<float>(rand())/static_cast<float>(RAND_MAX);
	}


	int newImageSize = m_OutputWidth*m_OutputHeight*m_OutputDepth;
	m_pActualOutput = std::unique_ptr<float>(new float[newImageSize]);
	m_Trainable= true;

	m_imageNum = m_filterItr=0;
}


void ConvolutionalNetwork::Predict(std::shared_ptr<float> data, int width,int height, int depth)
{
	assert( m_OutputWidth == width && "Output width doesn't match input width!");
	assert( m_OutputHeight == height && "Output height doesn't match input height!"); 

	// Create the new feature map
	int newImageSize = m_OutputWidth*m_OutputHeight*m_OutputDepth;
	
    m_pOutputBuffer = std::shared_ptr<float>( new float[newImageSize]);
	memset(m_pOutputBuffer.get(),0.0f,newImageSize*sizeof(float));
    memset(m_pActualOutput.get(),0.0f,newImageSize*sizeof(float));

    for (int layer = 0; layer < m_Filters.size(); ++layer)
    {
    	for ( int outX = 1; outX < m_OutputWidth -1; ++outX)
    	{
			for ( int outY = 1; outY < m_OutputHeight -1; ++outY)
			{	
				int outputIndex = outX + outY*m_OutputWidth + layer*m_OutputWidth*m_OutputHeight;

				float sum = 0.0f;
				for (int z = 0; z < depth; ++z)
				{
					for ( int u = 0; u < m_FilterSize; ++u)
			    	{
						for ( int v = 0; v < m_FilterSize; ++v)
						{
							int x = outX + u - 1;
							int y = outY + v - 1;

							int inputIndex = x + y*width + z*width*height;

							int kernelIndex = u + v*m_FilterSize + z*m_FilterSize*m_FilterSize;
							sum += data.get()[inputIndex]*m_Filters[layer].get()[kernelIndex];
						}
					}
				}
				sum += m_pBiases.get()[layer];

				m_pActualOutput.get()[outputIndex] = sum;
				m_pOutputBuffer.get()[outputIndex] = std::max(0.0f,sum);
			}
    	}
    }

    

}



static int savePNG(const char* filename, std::shared_ptr<float> &imageBuffer,int width,int height)
{
	/*std::vector<unsigned char> rawPixels,buffer;
	unsigned int error = 0;
	// Convert from 0.0f-1.0f to 0-255u and add in alpha
    for (int y = 0; y < height; ++y)
    {
        for (int x =0; x < width; ++x)
        {
            int index = y*width*3 + x*3;
            buffer.push_back(imageBuffer.get()[index]*255.0f);
            buffer.push_back(imageBuffer.get()[index+1]*255.0f);
            buffer.push_back(imageBuffer.get()[index+2]*255.0f);
            buffer.push_back(255);
        }
	}

	
	error = lodepng::encode(rawPixels, buffer, width, height);
	
	if(error)
	{
		printf("Error: %s\n",lodepng_error_text(error));
		return -1;
	}

	lodepng::save_file(rawPixels,filename);
	*/
	return 0;
}



void ConvolutionalNetwork::Print()
{



	for (int layer = 0; layer < m_Filters.size(); ++layer)
	{
		std::string filename;
		std::stringstream stream;

		stream << "image_" << m_imageNum << "_layer_" <<layer<< ".png";
		filename =stream.str();
		int depth = m_OutputWidth*m_OutputHeight*3;
		auto temp = std::shared_ptr<float> (new float[depth]);
		float max =0.0f;

		for (int i =0; i < m_OutputWidth*m_OutputHeight; ++i)
		{
			if ( m_pOutputBuffer.get()[i] > max)
			{
				max = m_pOutputBuffer.get()[i];
			}
		}
		
		for ( int outX = 0; outX < m_OutputWidth ; ++outX)
		{
			for ( int outY = 0; outY < m_OutputHeight; ++outY)
			{	
				int index = outX + outY*m_OutputWidth + layer*m_OutputWidth*m_OutputHeight;
				int outIndex = outX*3 + outY*m_OutputWidth*3;
				temp.get()[outIndex] = m_pOutputBuffer.get()[index]/max;
				temp.get()[outIndex+1] = m_pOutputBuffer.get()[index]/max;
				temp.get()[outIndex+2] = m_pOutputBuffer.get()[index]/max;
			}
		}


		savePNG(filename.c_str(),temp,m_OutputWidth,m_OutputHeight);


	/*	std::string filterFilename;
		std::stringstream filterstream;

		filterstream << "filter_" << layer << "_itr_" << m_filterItr << ".png";
		filterFilename =filterstream.str();
		auto tempFilter = std::shared_ptr<float> (new float[m_FilterSize*m_FilterSize*m_InputDepth]);
		
		for (int z = 0; z < m_InputDepth; ++z)
		{
			for ( int u = 0; u < m_FilterSize; ++u)
	    	{
				for ( int v = 0; v < m_FilterSize; ++v)
				{							
					int kernelIndex = u + v*m_FilterSize + z*m_FilterSize*m_FilterSize;


					tempFilter.get()[kernelIndex] = m_Filters[layer].get()[kernelIndex];
				}
			}	
		}

							

		savePNG(filterFilename.c_str(),tempFilter,m_FilterSize,m_FilterSize);*/
	} 
	++m_filterItr;
	++m_imageNum;

	/*for (int layer = 0; layer < m_Filters.size(); ++layer)
    {
    	printf("Filter[%d]\n",layer );
		for (int z = 0; z < m_InputDepth; ++z)
		{
			for ( int u = 0; u < m_FilterSize; ++u)
	    	{
				for ( int v = 0; v < m_FilterSize; ++v)
				{							
					int kernelIndex = u + v*m_FilterSize + z*m_FilterSize*m_FilterSize;

					printf("\tf[%d] = %f gradSum %f\n",kernelIndex,m_Filters[layer].get()[kernelIndex],m_GradientSum[layer].get()[kernelIndex] );
				}
			}
		}
	}*/
}


void ConvolutionalNetwork::Train(std::shared_ptr<float> input,std::shared_ptr<float> outputGradients, float label)
{
    const float learningRate = 0.01f;
	int filterVolume = m_FilterSize*m_FilterSize*m_InputDepth;

	memset(m_pGradients.get(),0.0f,sizeof(float)*m_InputVolume);

	for (int layer = 0; layer < m_Filters.size(); ++layer)
	{
		memset(m_GradientSum[layer].get(),0.0f,sizeof(float)*filterVolume);
	}


    for (int layer = 0; layer < m_Filters.size(); ++layer)
    {
    	for ( int outX = 1; outX < m_OutputWidth -1; ++outX)
    	{
			for ( int outY = 1; outY < m_OutputHeight -1; ++outY)
			{	
				int outputIndex = outX + outY*m_OutputWidth + layer*m_OutputWidth*m_OutputHeight;

				float gradient = 0.0f;

				if ( m_pOutputBuffer.get()[outputIndex] > 0.0f) 
				{
					gradient = outputGradients.get()[outputIndex];
				}

				//printf("Gradient %f\n",gradient );

				for (int z = 0; z < m_InputDepth; ++z)
				{
					for ( int u = 0; u < m_FilterSize; ++u)
			    	{
						for ( int v = 0; v < m_FilterSize; ++v)
						{
							int x = outX + u - 1;
							int y = outY + v - 1;

							int inputIndex = x + y*m_OutputWidth + z*m_OutputWidth*m_OutputHeight;

							int kernelIndex = u + v*m_FilterSize + z*m_FilterSize*m_FilterSize;

							m_pGradients.get()[inputIndex] += m_Filters[layer].get()[kernelIndex]*gradient;
							m_GradientSum[layer].get()[kernelIndex] += input.get()[inputIndex]*gradient;
						}
					}
				}
			}
    	}
    }


    for (int layer = 0; layer < m_Filters.size(); ++layer)
	{
		for (int z = 0; z < m_InputDepth; ++z)
		{
			for ( int u = 0; u < m_FilterSize; ++u)
	    	{
				for ( int v = 0; v < m_FilterSize; ++v)
				{
					int kernelIndex = u + v*m_FilterSize + z*m_FilterSize*m_FilterSize;

					m_Filters[layer].get()[kernelIndex] -= learningRate*(m_GradientSum[layer].get()[kernelIndex]*0.00001f);
				}
			}
		}
	}
}


std::shared_ptr<float> ConvolutionalNetwork::BackPass(std::shared_ptr<float> val) const
{

	std::shared_ptr<float> generatedInput = std::shared_ptr<float>(new float[m_InputWidth*m_InputHeight*m_InputDepth]);
	memset(generatedInput.get(),0.0f,m_InputWidth*m_InputHeight*m_InputDepth);

	for (int layer = 0; layer < m_Filters.size(); ++layer)
    {
    	for ( int outX = 0; outX < m_OutputWidth ; ++outX)
    	{
			for ( int outY = 0; outY < m_OutputHeight ; ++outY)
			{	
				int outputIndex = outX + outY*m_OutputWidth + layer*m_OutputWidth*m_OutputHeight;
				int generatedInputIndex = outX + outY*m_InputWidth;
				generatedInput.get()[generatedInputIndex] += val.get()[outputIndex]*m_Filters[layer].get()[0];
			}
    	}
    }


    return generatedInput;
}





