#include "Network.h"
#include "ConvolutionalNetwork.h"
#include "MaxPooling.h"
#include "LinearClassifier.h"
#include <stack>
#include <assert.h>
#include <cmath>
Network::Network(int outWidth,int outHeight,int outDepth):
	m_OutputWidth(outWidth),
	m_OutputHeight(outHeight),
	m_OutputDepth(outDepth),
	m_Trainable(false),
	m_Error(0.0f)
{ }


void Network::Predict(std::shared_ptr<float> inData, int inWidth,int inHeight, int inDepth)
{
	int width = inWidth;
	int height= inHeight;
	int depth = inDepth;
	std::shared_ptr<float> data = inData;
	int counter = 0;
	// For each layer in the network
	for (auto& net : m_NeuralNet)
	{
		// Forward propagate using the previous layers data
		net->Predict(data,width,height,depth);

		// Set the current data the result of the above forward propagate
		data = net->GetOutput(width,height,depth);

		if (counter++ == 0)
		{
			static_cast<ConvolutionalNetwork*>(net.get())->Print();
		}

	}

	m_pOutputBuffer = data;
}


void Network::AddMaxPooling(int areaSize)
{
	Network * previousLayer = this;

	if (m_NeuralNet.size() != 0)
	{
		previousLayer = m_NeuralNet.back().get();
	}

	m_NeuralNet.push_back(std::unique_ptr<Network>(new MaxPooling(areaSize,previousLayer)));
}


void Network::AddConvolution(int numberOfFilters, int filterSize,int stride)
{
	Network * previousLayer = this;

	if (m_NeuralNet.size() != 0)
	{
		previousLayer = m_NeuralNet.back().get();
	}

	m_NeuralNet.push_back(std::unique_ptr<Network>(new ConvolutionalNetwork(numberOfFilters,filterSize,previousLayer)));
}


void Network::AddLinearClassifier()
{
	Network * previousLayer = this;

	if (m_NeuralNet.size() != 0)
	{
		previousLayer = m_NeuralNet.back().get();
	}

	m_NeuralNet.push_back(std::unique_ptr<Network>(new LinearClassifier(previousLayer)));

}


std::shared_ptr<float> Network::GetOutput(int &width,int &height, int &depth) const
{
	if (m_NeuralNet.size() != 0)
	{
		return m_NeuralNet.back()->GetOutput(width,height,depth);
	}
	width = m_OutputWidth;
	height= m_OutputHeight;
	depth = m_OutputDepth;

	return m_pOutputBuffer;
}


std::shared_ptr<float> Network::Backpass(float val) const
{

	std::shared_ptr<float> ptr = nullptr;

	for (int layer = m_NeuralNet.size() - 1; layer >= 0; --layer)
	{
		auto &net = m_NeuralNet[layer];

		LinearClassifier * logit = dynamic_cast<LinearClassifier*>(net.get());

		if ( logit)
		{
			ptr = logit->BackPass(val);
		} else 
		{
			ConvolutionalNetwork * cov = dynamic_cast<ConvolutionalNetwork*>(net.get());

			if ( cov)
			{
				ptr = cov->BackPass(ptr);
			}
		}
	}

	return ptr;
}


void Network::Train(std::shared_ptr<float> input,int inWidth, int inHeight, int inDepth, float label)
{
	std::stack<std::shared_ptr<float>> data; data.push(input);
	
    int newWidth =inWidth, newHeight = inHeight, newDepth =inDepth;
    
	// Forward propagate, saving the state of the network
	for (auto& net : m_NeuralNet)
	{
		net->Predict(data.top(),newWidth,newHeight,newDepth);
		data.push(net->GetOutput(newWidth,newHeight,newDepth));
    }

	std::shared_ptr<float> x = nullptr;
    
    
    std::shared_ptr<float> gradients = nullptr;
    
	for (int layer = m_NeuralNet.size() - 1; layer >= 0; --layer)
	{
		auto & net = m_NeuralNet[layer];
		
        if ( !gradients )
        {
            gradients =data.top();
            data.pop();
        }
        
        x = data.top();
		data.pop();

        net->Train(x,gradients,label);
		gradients = net->GetGradients();
	}
}

void Network::Train(std::shared_ptr<float> input,std::shared_ptr<float> output, float label)
{

}

