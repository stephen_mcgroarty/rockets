#ifndef CONVOLUTIONAL_NETWORK_H
#define CONVOLUTIONAL_NETWORK_H
#include "Network.h"

class ConvolutionalNetwork:public Network
{
public:

	ConvolutionalNetwork(int numberOfFilters, int filterSize, Network* previousLayer);


	void Predict(std::shared_ptr<float> data, int width,int height, int depth) override;
	
	void Train(std::shared_ptr<float> input,std::shared_ptr<float> outputGradients, float label) override;

	// Passes result to the function and flows through weights to reproduce
	// a string which (the network estimates) could produce that result
	std::shared_ptr<float> BackPass(std::shared_ptr<float> val) const;

	void Print();

private:
	std::vector< std::unique_ptr<float> > m_Filters;
	std::vector< std::unique_ptr<float> > m_GradientSum;

	std::unique_ptr<float>  m_pBiases;
	std::unique_ptr<float>  m_pActualOutput;
	int m_FilterSize;
	int m_InputVolume;

	int m_InputDepth;
	int m_InputWidth,m_InputHeight;
	int m_imageNum;
	int m_filterItr;

};

#endif