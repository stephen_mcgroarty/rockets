#ifndef CAMERA_H
#define CAMERA_H
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Camera
{
public:
	Camera(const glm::vec3 & eye,const glm::vec3 & target,const glm::vec3 & up);


	void LookAt(glm::mat4 & returnedMatrice) const;

	

	void MoveForward(float amount);
	void MoveHorizontally(float amount);
	void MoveVertically(float amount);

private:
	glm::vec3 m_Eye, m_Target, m_Up, m_Right;	
};

#endif