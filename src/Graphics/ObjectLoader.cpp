#include "ObjectLoader.h"
#include <fstream>
#include <sstream>



ObjectLoader::ObjectLoader() { }
ObjectLoader::~ObjectLoader() { } 


void ObjectLoader::removeSlashes(std::string & s)
{
	while(s.find("/") != std::string::npos)
			s.replace(s.find("/"),1," ");
}




FaceFormat ObjectLoader::handleFace(const std::string& txt)
{
	if( txt.find("/") == std::string::npos )
		return V_ONLY;
	if( txt.find("//") != std::string::npos)
		return VN;
	if( txt.find("/",txt.find("/")+1) != std::string::npos)
		return VTN;

	return VT;

}

std::vector<float> ObjectLoader::generateNormals(const std::vector<float> & verts)
{

	std::vector<float> normals;
	for(unsigned int i =0; i < verts.size(); i+=9)
	{
		glm::vec3 p1(verts[i],verts[i+1],verts[i+2]);
		glm::vec3 p2(verts[i+3],verts[i+4],verts[i+5]);

		glm::vec3 result = glm::cross(p1,p2);
		result = glm::normalize(result);

		normals.push_back(result.x);
		normals.push_back(result.y);
		normals.push_back(result.z);
	}

	return normals;

}

Mesh * ObjectLoader::loadObject(const std::string & filePath)
{
	// load file into stream for efficency
	std::ifstream file(filePath,std::ios::binary | std::ios::beg | std::ios::in);
	std::stringstream stream;

	stream << file.rdbuf();

	file.close();

	std::vector<float> verts,uv,normals;
	std::vector<unsigned short> indices,uvIndices,normalIndices;

	FaceFormat format = FORMAT_NULL;

	for(std::string str = ""; stream.good(); stream >> str )
	{
		if( str.compare("") == 0 ) continue;


		if( str[0] == 'v') //if the first char is v it will either be reading uvs,normals or vertices
		{
			if(str[1] == 't' ) // uv coords
			{
				float u,v;
				stream >> u >> v;
				uv.push_back(u);
				uv.push_back(v);
			}
			else if(str[1] == 'n') // normals
			{
				float x,y,z;
				stream >> x >> y >> z;
				normals.push_back(x); normals.push_back(y); normals.push_back(z);
			}
			else // vertices
			{
				float x,y,z;
				stream >> x >> y >> z;
				verts.push_back(x); verts.push_back(y); verts.push_back(z);				
			}
		}
		else if( str[0] == 'f' )
		{
			std::string tmp,t2,t3;
			std::stringstream buffer;
			stream >> tmp >> t2 >> t3;

			if( format == FORMAT_NULL) 
				format = handleFace(tmp);

			removeSlashes(tmp); removeSlashes(t2); removeSlashes(t3);

			buffer << tmp << " "<< t2 << " " << t3;


			if( format == VTN )
			{
				while( buffer.good() )
				{
					GLuint index[3];
					buffer >> index[0] >> index[1] >> index[2];
					indices.push_back(index[0]-1);
					uvIndices.push_back(index[1]-1);
					normalIndices.push_back(index[2]-1);
				}

					
			} else if( format == V_ONLY )
			{
						
				while (buffer.good())
				{
					GLuint i;
					buffer >> i;
					indices.push_back(i-1);
				}
			} else if( format == VN )
			{
				while( buffer.good() )
				{
					GLuint index[2];
					buffer >> index[0] >> index[1];
					indices.push_back(index[0]-1);
					normalIndices.push_back(index[1]-1);
				}
			} else if( format == VT )
			{
				while( buffer.good() )
				{
					GLuint index[2];
					buffer >> index[0] >> index[1];
					indices.push_back(index[0]-1);
					uvIndices.push_back(index[1]-1);
				}
			}

		}
		else 
		{
			char ch[256];
			stream.getline(ch,256);
		}

	}
	

	if( verts.size() == 0) return NULL;

	if(indices.size() == 0) // if the object is not indexed, just return the data
	{
		if( normals.size() == 0 && uv.size() == 0)
			return new Mesh(verts,{},{},{});
		if( uv.size() == 0)
			return new Mesh(verts,{},normals,{});
		if( normals.size() == 0 )
			return new Mesh(verts,uv,{},{});

		return new Mesh(verts,uv,normals,{});	
	}

	/*
	*	Otherwise reorder the data
	*/

	int index =0;

	std::vector<float> outVerts,outNorms,outUv;
	std::vector<int> outIndices;

	for(int i =0; i < indices.size(); i++)
	{
		unsigned int vertexIndice = indices[i];
		unsigned int normalIndice;
		unsigned int textIndice;

		if( normalIndices.size() != 0 ) normalIndice = normalIndices[i]; else normalIndice =0;
		if( uvIndices.size() != 0 ) textIndice = uvIndices[i];  else textIndice= 0;

		if( vertexIndice != 0 ) vertexIndice *= 3;
		if( textIndice   != 0 ) textIndice   *= 2;
		if( normalIndice != 0 ) normalIndice *= 3;

		outVerts.push_back( verts[vertexIndice]   );
		outVerts.push_back( verts[vertexIndice+1] );
		outVerts.push_back( verts[vertexIndice+2] );
		
		if( normalIndices.size() > normalIndice + 2)
		{
			outNorms.push_back( normals[normalIndice]  );
			outNorms.push_back( normals[normalIndice+1]);
			outNorms.push_back( normals[normalIndice+2]);
		}


		if( uvIndices.size() > textIndice+1)
		{
			outUv.push_back( uv[textIndice]);
			outUv.push_back( uv[textIndice+1]);
		}

		outIndices.push_back( index++);
	}

	if( outNorms.size() == 0 )	outNorms = generateNormals(outVerts);

	return new Mesh(outVerts,outUv,outNorms,{});
}

