#include <stdio.h>
#include "Graphics/Mesh.h"


Mesh::Mesh(): m_Indices(false)
{

}


Mesh::Mesh(const std::vector<float>& data,const std::vector<float>& uv,const std::vector<float>& normals,const std::vector<int>& indices): m_Indices(false)
{
	Finalize(data,uv,normals,indices);
	m_Size = data.size();
}


void Mesh::Finalize()
{	
	Finalize(m_TempVertexBuffer,m_TempUVBuffer,{},{});
}


void Mesh::Finalize(const std::vector<float>& data,const std::vector<float>& uv,const std::vector<float>& normals,const std::vector<int>& indices)
{
	glGenVertexArrays(1, &m_VertexArrayID);
	glBindVertexArray(m_VertexArrayID);

	GLenum err = glGetError();
	if( err != GL_NO_ERROR )
	{
		fprintf(stderr,"Error: %d Thrown in VertexArray creation\n",err);
	}

	glGenBuffers(1, &m_VertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_VertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, data.size()*sizeof(float), &data[0], GL_STATIC_DRAW);

	err = glGetError();
	if( err != GL_NO_ERROR )
	{
		fprintf(stderr,"Error: %d Thrown in VertexBuffer creation\n",err);
	}


	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_VertexBuffer);
	glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE,0,(void*)0);


	if (uv.size() > 0)
	{
		glGenBuffers(1, &m_UVBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_UVBuffer);
		glBufferData(GL_ARRAY_BUFFER, uv.size()*sizeof(float), &(uv[0]), GL_STATIC_DRAW);
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, m_UVBuffer);
		glVertexAttribPointer(1,2,GL_FLOAT, GL_FALSE,0,(void*)0);

		err = glGetError();
		if( err != GL_NO_ERROR )
		{
			fprintf(stderr,"Error: %d Thrown in UV buffer creation\n",err);
		}		
	}


	if (normals.size() > 0)
	{
		glGenBuffers(1, &m_NormalBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_NormalBuffer);
		glBufferData(GL_ARRAY_BUFFER, normals.size()*sizeof(float), &(normals[0]), GL_STATIC_DRAW);
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, m_NormalBuffer);
		glVertexAttribPointer(2,3,GL_FLOAT, GL_FALSE,0,(void*)0);

		err = glGetError();
		if( err != GL_NO_ERROR )
		{
			fprintf(stderr,"Error: %d Thrown in normal buffer creation\n",err);
		}		
	}

	if( indices.size() > 0)
	{
		m_Indices = true; //we are using indexed drawing
		m_IndiceSize = indices.size();
		glGenBuffers(1,&m_IndicesBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,m_IndicesBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,indices.size()*sizeof(int),&(indices[0]),GL_STATIC_DRAW);
	}

	m_Size = data.size();
}



void Mesh::AddVertice(const glm::vec3 & vertice)
{
	m_TempVertexBuffer.push_back(vertice.x);
	m_TempVertexBuffer.push_back(vertice.y);
	m_TempVertexBuffer.push_back(vertice.z);
}


void Mesh::AddUV(const glm::vec2 & UV)
{
	m_TempUVBuffer.push_back(UV.x);
	m_TempUVBuffer.push_back(UV.y);
}

Mesh::~Mesh()
{
	glDeleteVertexArrays(1,&m_VertexArrayID);
	glDeleteBuffers(1,&m_VertexBuffer);
	glDeleteBuffers(1,&m_UVBuffer);
	glDeleteBuffers(1,&m_IndicesBuffer);
	glDeleteBuffers(1,&m_NormalBuffer);
}


void Mesh::Render() const
{
	glEnableVertexAttribArray(0);
	glBindVertexArray(m_VertexArrayID);

	if (!m_Indices)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_VertexBuffer);

		glDrawArrays(GL_TRIANGLES, 0, m_Size);

		GLenum err = glGetError();
		if( err != GL_NO_ERROR )
		{
			fprintf(stderr,"Error: %d Thrown in glDrawArrays",err);
		}
	} else 
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,m_IndicesBuffer);
		glDrawElements(GL_TRIANGLES,m_IndiceSize,GL_UNSIGNED_INT,0);
	}

	glDisableVertexAttribArray(0);
}
