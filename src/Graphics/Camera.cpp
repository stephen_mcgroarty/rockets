#include "Graphics/Camera.h"
#define DEG_TO_RADIAN 0.017453293


Camera::Camera(const glm::vec3 & eye,const glm::vec3 & target,const glm::vec3 &up): m_Eye(eye),m_Target(target),m_Up(up)
{
	m_Right = glm::cross( glm::normalize(m_Eye - m_Target),up);
}


void Camera::LookAt(glm::mat4 & returnedMatrice) const
{
	returnedMatrice = glm::lookAt(m_Eye,m_Target,m_Up);
}


void Camera::MoveForward(float distance)
{
	glm::vec3 at = glm::normalize(m_Eye - m_Target) ;
	m_Eye += at * distance;
	m_Target += at*distance;
}


void Camera::MoveHorizontally(float distance)
{
	m_Eye += m_Right*distance;
	m_Target += m_Right*distance;
}


void Camera::MoveVertically(float distance)
{
	m_Eye += m_Up*distance;
	m_Target += m_Up*distance;
}