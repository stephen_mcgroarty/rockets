#ifndef TRAINER_H
#define TRAINER_H
#include <string>
#include <memory>
#include <map>
#include "Network/Network.h"

class Trainer
{
public:
	Trainer();

	void TrainNetwork();

	std::string GenerateString() const;

	void AddString(const std::string & str,float label);
private:

	std::unique_ptr<Network> m_pNetwork;
	std::map<std::string,int> m_DataSet;
};

#endif