#include <stdio.h>
#include "Application.h"


int main(int argc,char *argv[])
{
	const char * resourcePath = NULL;
	if ( argc != 1 ) resourcePath = argv[1];


	Application app {resourcePath};

	app.Run();

	return 0;
}
