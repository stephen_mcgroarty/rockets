#include "Trainer.h"
#include <iostream>
#include <fstream>
#include <string>
#include "Application.h"

Trainer::Trainer()
{
	 // Width, height, and depth (3 due to RGB) of input image
	m_pNetwork = std::unique_ptr<Network>(new Network(50,1,1));

	// Layer 1 - Convolution + ReLu
	m_pNetwork->AddConvolution(10,1,1);

	// Layer 3 - Linear Classifier
	m_pNetwork->AddLinearClassifier();
}

void Trainer::TrainNetwork()
{
	std::string filepath = std::string(g_ResourcePath) + std::string("Results.txt");

	std::ifstream file(filepath, std::ios::binary | std::ios::in);


	while (file.good())
	{
		std::string buf;
		int val = -1;

		file >> buf >> val;

		int sizeOfString = buf.size();

		for ( int i =0; i < 50 - sizeOfString; ++i)
		{
			buf += std::string("N");
		}

		m_DataSet.insert({buf,val});
	}
	file.close();
	for ( int itr = 0; itr < 1000; ++itr)
	{
		for ( auto & pair : m_DataSet)
		{
			std::shared_ptr<float> ptr { new float[50]};

			for (int i =0; i < 50; ++i)
			{
				ptr.get()[i] = (float)pair.first[i];
			}

			m_pNetwork->Train(ptr,50,1,1,(float)pair.second);
		}
	}
}

std::string Trainer::GenerateString() const
{
	auto generated = m_pNetwork->Backpass(1.0f);
	std::string str;
	for (int i =0; i < 50; ++i)
	{
		float val = generated.get()[i];
		printf(" %f",val);
		if (val < -1.0f)
		{
			str += 'F';
		} 
		else if ( val < 0.0f)
		{
			str += 'S';
		}
		else if ( val > 50.0f)
		{
			str += 'N';
		} 
		else 
		{
			str += 'E';
		}
	}

	return str;
}



void Trainer::AddString(const std::string &str, float label)
{
	m_DataSet.insert({str, label});

	std::string filepath = std::string(g_ResourcePath) + std::string("Results.txt");
	std::ofstream file(filepath, std::ios::binary);

	int i = 0;

	for ( auto & pair : m_DataSet)
	{
		file << pair.first << " " << pair.second;
		if ( i != m_DataSet.size()-1)
		{
			file << std::endl;
		}

		++i;
	}

	file.close();
}