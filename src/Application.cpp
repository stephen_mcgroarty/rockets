#include "Application.h"
#include <string>
#include <glm/gtx/vector_angle.hpp>

const char * g_ResourcePath = "../Resources/";

Application::Application(const char * resourcePath)
{
	if (resourcePath != NULL) g_ResourcePath = resourcePath;

	m_pRenderer = std::unique_ptr<Renderer>(new Renderer(800,800));
	
    m_pProgram = std::shared_ptr<ShaderProgram>(new ShaderProgram);
    
    std::string buffer = std::string(g_ResourcePath) + std::string("default.vert");
	m_pProgram->LoadShader(ShaderProgram::VERTEX,buffer.c_str() );

    buffer = std::string(g_ResourcePath) + std::string("default.frag");
	m_pProgram->LoadShader(ShaderProgram::FRAGMENT,buffer.c_str());
	m_pProgram->Finalize();



	std::shared_ptr<ShaderProgram> backgroundProgram = std::shared_ptr<ShaderProgram>(new ShaderProgram);
    buffer = std::string(g_ResourcePath) + std::string("background.vert");
	backgroundProgram->LoadShader(ShaderProgram::VERTEX,buffer.c_str());

	buffer = std::string(g_ResourcePath) + std::string("background.frag");
	backgroundProgram->LoadShader(ShaderProgram::FRAGMENT,buffer.c_str());

	backgroundProgram->Finalize();
    

	m_Up = glm::vec3(0.0f,1.0f,0.0f);
	m_Eye  =  glm::vec3(0.0f,0.0f,-5.0f);

	glm::vec3 right = glm::cross(m_Up,glm::normalize(m_Eye));
	m_Target = m_Eye + glm::cross(m_Up,right);

	float angle = 0.0f;

	m_Projection = glm::perspective(45.0f,800.0f/800.0f,0.1f,4000.0f);

	m_pCamera = std::unique_ptr<Camera>(new Camera(m_Eye,m_Target,m_Up));


	m_pBackground = std::unique_ptr<Background>( new Background(backgroundProgram));

	m_Trainer.TrainNetwork();
	m_RocketString = m_Trainer.GenerateString();
	m_pRocket = std::shared_ptr<Rocket>(new Rocket(m_RocketString.c_str(),m_pProgram,m_Target,angle,m_Up));
}


void Application::Render()
{
	glm::mat4 view;
	m_pCamera->LookAt(view);
	glm::mat4 VP = m_Projection*view;

	m_pRenderer->Clear();

	m_pBackground->Render(m_pRocket->GetDrag());
	m_pRocket->Render(VP);

	m_pRenderer->SwapBuffers();
}


void Application::Run()
{
	bool running = true;
	while(running)
	{
		if (!m_pRenderer->CheckWindowEvents())
		{
			running = false;
		}

		Render();
        m_pRocket->Update();

		const Uint8 * keys = SDL_GetKeyboardState(NULL);

		if( keys[SDL_SCANCODE_ESCAPE] )
		{
			SDL_Quit();
		}

		m_pCamera->MoveHorizontally( m_pRocket->GetVelocity().x);
		m_pCamera->MoveVertically( m_pRocket->GetVelocity().y);

		if( keys[SDL_SCANCODE_W] ) m_pCamera->MoveForward(-1.0f);//MoveForward(m_Eye,m_Target,0.1f,m_Yaw,m_Pitch);// }
		if( keys[SDL_SCANCODE_S] ) m_pCamera->MoveForward( 1.0f);// MoveForward(m_Eye,m_Target,-0.1f,m_Yaw,m_Pitch); }
		
		if( keys[SDL_SCANCODE_A] ) m_pCamera->MoveHorizontally( 1.0f);
		if( keys[SDL_SCANCODE_D] ) m_pCamera->MoveHorizontally(-1.0f);

		if( keys[SDL_SCANCODE_Z] ) m_pCamera->MoveVertically(-1.0f);
		if( keys[SDL_SCANCODE_X] ) m_pCamera->MoveVertically( 1.0f);
	
		if (!m_pRocket->CanFire())
		{
			float val = 0.0f;

			if ( m_pRocket->GetPosition().y > 12000.0f)
			{
				val = 1.0f;
			}

			m_Trainer.AddString( m_RocketString,val);
			running = false;
		}
	}
}