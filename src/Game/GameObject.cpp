#include "Game/GameObject.h"
#include "Graphics/ObjectLoader.h"
#include <SDL.h>
#include <string>
#include "Application.h"

GameObject::GameObject(const glm::vec3 & position,
					  std::shared_ptr<ShaderProgram> program,
					  std::shared_ptr<ShaderProgram> normal,
                      const char * filename,
                      float scale)
							: m_Position(position),
                              m_Scale(glm::vec3(scale)),
							  m_pProgram(program),
							  m_pNormalProgram(normal),
							  m_pMesh(nullptr),
							  m_Rotate(false)
{
	ObjectLoader obj;

	std::string file {g_ResourcePath};
	file = file + std::string("Models/") + filename;
	m_pMesh = std::shared_ptr<Mesh>(obj.loadObject(file.c_str()));

	if (!m_pMesh)
	{
		fprintf(stderr, "Failed to load file: %s \n",file.c_str());
	}

	m_PivotPoint = glm::vec3({0.0f,0.0f,0.0f});
	m_RotationAxis = glm::vec3({1.0f,0.0f,0.0f});

}


void GameObject::Render(glm::mat4 &PV)
{
    m_pProgram->UseProgram();

	glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model,m_Position);

    if ( m_Rotate )
    {
	    glm::vec3 pivot =  m_PivotPoint - m_Position;
	    model = glm::translate(model,pivot);
	    model = glm::rotate(model,SDL_GetTicks()/1000.0f,m_RotationAxis);
	    model = glm::translate(model,-pivot);
    }

    model = glm::scale(model,m_Scale);


	model = PV*model;

	// TODO: Move this.
	m_pProgram->SetUniformMatrix4fv("MVP",1,false,glm::value_ptr(model));

	if (m_pMesh)
	{
		m_pMesh->Render();
	}
}


void GameObject::Update() { }
