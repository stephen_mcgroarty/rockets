#ifndef ENGINE_H
#define ENGINE_H
#include "Game/GameObject.h"


class Engine: public GameObject
{
public:
    Engine(const glm::vec3 & position,std::shared_ptr<ShaderProgram> program);
    
    void Render(glm::mat4 &PV) override;
    
    
    float GetThrust() const { return m_Thrust; }
    float GetBurnRate() const { return m_BurnRate; }

private:
    
    float m_Thrust;
    float m_BurnRate;

};


#endif