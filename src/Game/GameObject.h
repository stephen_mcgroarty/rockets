#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H
#include "Graphics/Mesh.h"
#include "Graphics/ShaderProgram.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class GameObject 
{
public:
	GameObject(const glm::vec3 & position,std::shared_ptr<ShaderProgram> program,const std::shared_ptr<ShaderProgram> normal,const char * filename,float scale = 1.0f);

	virtual void Render(glm::mat4 &PV);

	virtual void Update();
    
    float GetWeight() const { return m_Weight; }
    
    glm::vec3 GetPosition() const{ return m_Position;}
    glm::vec3 & GetPosition() {return m_Position;}
    
    glm::vec3 GetScale() const{ return m_Scale;}
    glm::vec3 & GetScale() {return m_Scale;}


protected:
	glm::vec3 m_Position;
    glm::vec3 m_Scale;
    
	std::shared_ptr<ShaderProgram> m_pProgram,m_pNormalProgram;
	std::shared_ptr<Mesh> m_pMesh;
    float m_Weight;

    glm::vec3 m_PivotPoint;
    glm::vec3 m_RotationAxis;
    bool m_Rotate;

};

#endif