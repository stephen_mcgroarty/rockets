#include "Background.h"
#include "Graphics/ObjectLoader.h"
#include "Application.h"


Background::Background(std::shared_ptr<ShaderProgram> program): m_pProgram(program)
{
	ObjectLoader obj;

	std::string file {g_ResourcePath};
	file = file + std::string("Models/cube.obj");
	m_pMesh = std::shared_ptr<Mesh>(obj.loadObject(file.c_str()));

	if (!m_pMesh)
	{
		fprintf(stderr, "Failed to load file: %s \n",file.c_str());
	}
    m_pProgram->UseProgram();
}


void Background::Render(float drag)
{
	glEnable(GL_DEPTH_TEST);
    m_pProgram->UseProgram();
	m_pProgram->SetUniform1f("drag",drag);

	if (m_pMesh)
	{
		m_pMesh->Render();
	}

	glDisable(GL_DEPTH_TEST);
}
