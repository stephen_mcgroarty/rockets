#ifndef FUEL_TANK_H
#define FUEL_TANK_H
#include "Game/GameObject.h"


class FuelTank: public GameObject
{
public:
    FuelTank(const glm::vec3 & position,std::shared_ptr<ShaderProgram> program);
    
    void Render(glm::mat4 &PV) override;
    

    void BurnFuel(float burnRate);

    bool IsEmpty() const { return m_Empty; }
private:
	float m_DryWeight;
	bool m_Empty;
};


#endif