#ifndef Rocket_h
#define Rocket_h
#include "Game/GameObject.h"


class Rocket
{
public:
    Rocket(const char * str,std::shared_ptr<ShaderProgram> program,const glm::vec3 & position,float angle,const glm::vec3 & ref);
    
    
    void Render(glm::mat4 &PV) const;


    void CreateRocket(const char * rocket);
    
    void Update();

    const glm::vec3 & GetVelocity() const { return m_Velocity; } 

    float GetDrag() const {return m_Drag; }

    const glm::vec3 & GetPosition() const { return m_Position; }

    bool CanFire() const { return m_CanFire; }
private:
    glm::vec3 m_Position;
    std::shared_ptr<ShaderProgram> m_pProgram;
    float m_Angle;

    glm::vec3 m_Up;


    std::vector<std::vector<std::shared_ptr<GameObject>>> m_Stages;


    glm::vec3 m_Velocity;
    int m_CurrentStage;

    float m_Drag;
    bool m_CanFire;
};


#endif /* Rocket_h */
