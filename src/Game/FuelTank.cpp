#include "Game/FuelTank.h"

FuelTank::FuelTank(const glm::vec3 & position,std::shared_ptr<ShaderProgram> program):
    GameObject(position,program,nullptr,"cylinder.obj",1.0f)
{
    m_Weight = 1000.0f;
    m_DryWeight = 0.001f;
    m_Empty = false;
}


void FuelTank::BurnFuel(float burnRate)
{
	float actualWeight = m_Weight - m_DryWeight;
	if ( burnRate > actualWeight )
	{
		m_Weight = m_DryWeight;
		m_Empty  = true;
	} else 
	{
		m_Weight -= burnRate;
	}
}


void FuelTank::Render(glm::mat4 &PV)
{
	m_pProgram->UseProgram();
    static float col[4] = {0.0f,1.0f,0.0f,1.0f};
    m_pProgram->SetUniform4fv("colour",1,col);
    
    GameObject::Render(PV);
}
