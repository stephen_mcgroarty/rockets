#ifndef PLANET_H
#define PLANET_H
#include "Game/GameObject.h"
#include <GL/glew.h>

/*
void decodeOneStep(const char* filename)
{
  unsigned error;
  unsigned char* image;
  unsigned width, height;

  error = lodepng_decode32_file(&image, &width, &height, filename);
  if(error) printf("error %u: %s\n", error, lodepng_error_text(error));


  free(image);
}
*/

class Planet: public GameObject
{
public:
	Planet(std::shared_ptr<ShaderProgram> program,std::shared_ptr<ShaderProgram> orbit,float scale);

    void Render(glm::mat4 &PV) override;

private:
	std::shared_ptr<ShaderProgram> m_pOrbit;
	GLuint m_TextureID;
};

#endif