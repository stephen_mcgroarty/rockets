#include "Game/Payload.h"


Payload::Payload(const glm::vec3 & position,std::shared_ptr<ShaderProgram> program):
	GameObject(position,program,nullptr,"cylinder.obj",1.0f)
{
	m_Weight = 500.0f;
}

void Payload::Render(glm::mat4 &PV)
{
	m_pProgram->UseProgram();
    static float col[4] = {0.5f,0.5f,0.5f,1.0f};
    m_pProgram->SetUniform4fv("colour",1,col);
    
    GameObject::Render(PV);
}