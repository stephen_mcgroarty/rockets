#ifndef BACKGROUND_H
#define BACKGROUND_H
#include "Graphics/Mesh.h"
#include "Graphics/ShaderProgram.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


class Background
{
public:
	Background(std::shared_ptr<ShaderProgram> program);
    void Render(float drag);

private:
	std::shared_ptr<ShaderProgram> m_pProgram;
	std::shared_ptr<Mesh> m_pMesh;

};


#endif

