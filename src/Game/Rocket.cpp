#include "Game/Rocket.h"
#include "Game/Engine.h"
#include "Game/FuelTank.h"
#include "Graphics/ShaderProgram.h"
#include "Game/Payload.h"
#include <cmath>


Rocket::Rocket(const char * str,std::shared_ptr<ShaderProgram> program,const glm::vec3 & position,float angle,const glm::vec3 & up):
            m_Position(position),m_pProgram(program),m_Angle(angle),m_Up(up),m_CurrentStage(0)
                              
{
    m_Drag = 0.0f;

    m_CanFire = true;
    CreateRocket(str);

}

void Rocket::Render(glm::mat4 &PV) const
{
    for ( auto &stage : m_Stages)
    {
        for (auto part : stage)
        {
            part->Render(PV);
        }
    }
}



void Rocket::CreateRocket(const char * rocket)
{
    m_Stages.push_back({});
    glm::mat4 model(1.0f);

    model = glm::rotate(model,-m_Angle,m_Up);

    glm::vec3 stagePointer {m_Up};
    const char * cur = rocket;

    while ( *cur != '\0')
    {
        glm::vec3 vec = glm::vec3(model*glm::vec4(stagePointer,1.0f)) +m_Position;
        std::shared_ptr<GameObject> part;

        if ( *cur == 'E' ) 
        {
            part = std::shared_ptr<GameObject>(new Engine(vec,m_pProgram));
            m_Stages.back().push_back(part);
        } 
        else if (*cur =='F')
        {
            part = std::shared_ptr<GameObject>(new FuelTank(vec,m_pProgram));
            m_Stages.back().push_back(part);
            stagePointer += m_Up;
        } 
        else if (*cur == 'S')
        {
            m_Stages.push_back({});
        }
        else if (*cur == 'N')
        {
            break;
        }

        ++cur;
    }

    glm::vec3 vec = glm::vec3(model*glm::vec4(stagePointer,1.0f)) +m_Position;
    std::shared_ptr<GameObject> part = std::shared_ptr<GameObject>(new Payload(vec,m_pProgram));
    m_Stages.back().push_back(part);
    stagePointer += m_Up;    
}


void Rocket::Update()
{
    float weight = 0.0f;
    
    for (int index = m_CurrentStage; index < m_Stages.size(); ++index)
    {
        auto &stage = m_Stages[index];
        for (auto part : stage)
        {
            weight += part->GetWeight();
        }
    }
    
    float fuelBurnt =0.0f;
    float thrust = 0.0f;
    bool empty = true;
    for (auto part : m_Stages[m_CurrentStage])
    {
        auto ptr = std::dynamic_pointer_cast<Engine>(part);
        if ( ptr != nullptr)
        {
            thrust += ptr->GetThrust();
            fuelBurnt+= ptr->GetBurnRate();
        } else 
        {
            auto ptrFuel = std::dynamic_pointer_cast<FuelTank>(part);
            if ( ptrFuel != nullptr)
            {
                ptrFuel->BurnFuel(fuelBurnt);

                if (!ptrFuel->IsEmpty())
                {
                    empty = false;
                }
            }
        }
    }

    if ( empty && m_CurrentStage != m_Stages.size()-1 )
    {
        ++m_CurrentStage;
    } else if (empty)
    {
        m_CanFire = false;
    }


    fuelBurnt /= 3.0f;
    
    // a = F/m
    
    // Just pretend that thrust is force for now
    glm::vec3 velocity = (thrust/weight)*m_Up;

    float drag = 1.0f - (1.0f/exp(velocity.y*velocity.y*velocity.y))*(1.0f/exp(m_Position.y/200.0f));

    m_Drag = drag;
    // Constant gravity term
    velocity = velocity*drag - m_Up*0.01f*drag;

    bool positionSet = false;
    for (int index = 0; index < m_Stages.size(); ++index)
    {
        auto &stage = m_Stages[index];

        if ( index >= m_CurrentStage && m_CanFire)
        {
            for (auto part : stage)
            {
                part->GetPosition() += velocity;

                if (!positionSet)
                {
                    m_Position = part->GetPosition();
                    positionSet = true;
                }
            }
        } 
        else 
        {
            for (auto part : stage)
            {        
                part->GetPosition() -= m_Up*0.01f;
            }  
        }
    }

    m_Velocity = velocity;


    printf("%f %f %f\n",m_Position.x,m_Position.y,m_Position.z);
    printf(" %f\n %f\n %f %f %f\n",weight,drag,velocity.x,velocity.y,velocity.z ); 

}