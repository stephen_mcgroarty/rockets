#ifndef PAYLOAD_H
#define PAYLOAD_H
#include "Game/GameObject.h"

class Payload: public GameObject
{
public:
    Payload(const glm::vec3 & position,std::shared_ptr<ShaderProgram> program);
    
    void Render(glm::mat4 &PV) override;
};


#endif