#include "Game/Planet.h"
#include "Graphics/lodepng.h"
#include <SDL.h>
#include "Application.h"

Planet::Planet(std::shared_ptr<ShaderProgram> program,std::shared_ptr<ShaderProgram> orbit,float scale):
	GameObject({0.0f,0.0f,0.0f},program,nullptr,"Sphere.obj",scale)
{
	unsigned error;
	unsigned char* image;
	unsigned width, height;

	std::string buffer {g_ResourcePath};
	buffer = buffer + std::string("Textures/earth_day.png");
	error = lodepng_decode32_file(&image, &width, &height, buffer.c_str());
	if(error) 
	{
		printf("error %u: %s\n", error, lodepng_error_text(error));
	}


	glGenTextures(1, &m_TextureID);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, m_TextureID);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	free(image);

	m_PivotPoint = glm::vec3(m_Position);
	m_RotationAxis = glm::vec3(-0.39f,0.92f,0.0f);
	m_Rotate = false;

	m_pOrbit = orbit;
}

void Planet::Render(glm::mat4 &PV)
{

	m_pProgram->UseProgram();
	glBindTexture(GL_TEXTURE_2D, m_TextureID);
    GameObject::Render(PV);
/*
    m_pOrbit->UseProgram();

	glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model,m_Position);

    if ( m_Rotate )
    {
	    glm::vec3 pivot =  m_PivotPoint - m_Position;
	    model = glm::translate(model,pivot);
	    model = glm::rotate(model,SDL_GetTicks()/1000.0f,m_RotationAxis);
	    model = glm::translate(model,-pivot);
    }

    model = glm::scale(model,m_Scale);

	model = PV*model;

	// TODO: Move this.
	m_pOrbit->SetUniformMatrix4fv("MVP",1,false,glm::value_ptr(model));

	if (m_pMesh)
	{
		m_pMesh->Render();
	}*/
}
