#include "Game/Engine.h"


Engine::Engine(const glm::vec3 & position,std::shared_ptr<ShaderProgram> program):
    GameObject(position,program,nullptr,"cylinder.obj",0.3f)
{
    m_Weight = 1.0f;
    m_Thrust = 500.0f;
    m_BurnRate= 0.1f;
}


void Engine::Render(glm::mat4 &PV)
{
	m_pProgram->UseProgram();
    static float col[4] = {1.0f,0.0f,0.0f,1.0f};
    m_pProgram->SetUniform4fv("colour",1,col);
    
    GameObject::Render(PV);
}
