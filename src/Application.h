#ifndef APPLICATION_H
#define APPLICATION_H
#include "Graphics/Renderer.h"
#include "Game/GameObject.h"
#include "Game/Rocket.h"
#include "Graphics/Camera.h"
#include "Game/Background.h"
#include "Trainer.h"

class Application
{
public:

	Application(const char * resourcePath);

	void Run();

private:
	std::unique_ptr<Renderer> m_pRenderer;
	std::shared_ptr<ShaderProgram> m_pProgram;
	std::shared_ptr<Rocket> m_pRocket;

	glm::vec3 m_Eye;
	glm::vec3 m_Target;
	glm::vec3 m_Up;
	glm::mat4 m_Projection;
	void Render();

	std::unique_ptr<Camera> m_pCamera;
	std::unique_ptr<Background> m_pBackground;
	Trainer m_Trainer;


	std::string m_RocketString;
};


extern const char * g_ResourcePath;

#endif