import subprocess
import matplotlib.pyplot as plt


exePath = r"/Users/Stephen/Projects/Rockets/build/Rockets"
resourcePath = r"/Users/Stephen/Projects/Rockets/Resources/"

heightOverTime = []
weightOverTime = []

dragOverTime = []

velocityOverTime = []



exe = subprocess.Popen([exePath,resourcePath], stdout=subprocess.PIPE,stderr=subprocess.PIPE,stdin=subprocess.PIPE)
output = exe.communicate()[0].split()
rawOutput = zip(*[iter(output)]*8)


for sublist in rawOutput:
	heightOverTime += [sublist[1]]
	weightOverTime += [sublist[3]]
	dragOverTime   += [sublist[4]]
	velocityOverTime+=[sublist[6]]

plt.figure(1)
plt.plot(heightOverTime,'b')
plt.axhline(y=heightOverTime[len(heightOverTime)-1],color='r')

# EF = 33358
plt.ylabel('Position')
plt.xlabel('time')
plt.title('Position over Time (max reached ' + heightOverTime[len(heightOverTime)-1] + ')')

plt.show(block=True)

plt.figure(2)
plt.plot(weightOverTime,'r')
plt.ylabel('Weight')
plt.xlabel('time')

plt.title('Weight over time')

plt.show(block=True)

plt.figure(3)
plt.plot(dragOverTime,'g')
plt.ylabel('drag')
plt.xlabel('time')

plt.title('Drag over time')

plt.show(block=True)

plt.figure(4)
plt.plot(velocityOverTime,'c')
plt.ylabel('velocity')
plt.xlabel('time')

plt.title('Velocity over time')

plt.show(block=True)


plt.figure(5)
plt.plot(velocityOverTime[0:len(velocityOverTime)-100],'c')
plt.ylabel('velocity')
plt.xlabel('time')

plt.title('Velocity over time (minus last 100)')

plt.show(block=True)


f, axarr = plt.subplots(4,sharex=True)
axarr[0].plot(heightOverTime,'b',label='Altitude')
axarr[1].plot(weightOverTime,'r',label='Weight')
axarr[2].plot(dragOverTime,'g',label='Drag')
axarr[3].plot(velocityOverTime,'c',label='Velocity')

plt.show(block=True)
