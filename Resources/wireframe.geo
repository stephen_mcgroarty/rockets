#version 410 core


layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

//uniform mat4 MVP;
//uniform mat3 normalMatrix;
//in vec4 te_position[3];

//out vec3 geo_face_normal;

void main()
{
/*
	vec4 A = te_position[2] - te_position[0];
    vec4 B = te_position[1] - te_position[0];
    geo_face_normal = normalMatrix * normalize(cross(vec3(A), vec3(B)));
*/   

    gl_Position = gl_in[0].gl_Position; 
    EmitVertex();
    
    gl_Position = gl_in[1].gl_Position; 
    EmitVertex();

    gl_Position = gl_in[2].gl_Position; 
    EmitVertex();

    EndPrimitive();
}