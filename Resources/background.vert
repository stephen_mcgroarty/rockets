#version 410 core
layout(location = 0) in vec4 model;
layout(location = 1) in vec2 uv;

out vec2 UV;


void main()
{
	gl_Position= model;
    UV = uv;
}