#version 410 core
layout(location = 0) in vec4 model;
layout(location = 2) in vec3 n;

out Vertex
{
	vec3 normal;
} vertex;

void main()
{
	gl_Position= model;
	vertex.normal = n;
}