#version 410 core
layout(triangles) in;
layout(line_strip, max_vertices=90) out;

in vec3 vertNormal[3];
uniform mat4 MVP;

uniform float speed = 0.1;
uniform float direction = 90;

uniform vec3 centre = vec3(1.0f,-1.0f,-1.0f);
mat4 rotationMatrix(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;
    
    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}


void main()
{
    if (gl_PrimitiveIDIn == 0)
    {
        vec4 position = gl_in[0].gl_Position;
        vec3 target = vertNormal[0];
        vec3 normal = vertNormal[0];
        
        mat4 rot = rotationMatrix(vec3(1.0,0.0,0.0),direction);
        target = (rot*vec4(target,1.0)).xyz;

        const int num_iterations = 45;
        for (int i =0; i < num_iterations; ++i)
        {  

            vec3 velocity = smoothstep(normal,target,vec3(i/num_iterations,i/num_iterations,i/num_iterations))*speed;
            gl_Position = MVP* position;
            EmitVertex();

            position.xyz += velocity;

            gl_Position = MVP* position;
            EmitVertex();
            EndPrimitive();
        }

    }
}