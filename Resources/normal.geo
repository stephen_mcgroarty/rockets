#version 410 core
layout(triangles) in;
layout(line_strip, max_vertices=6) out;

in Vertex
{
	vec3 normal;
} vertex[];

uniform mat4 MVP;

void main()
{
	for(int vertexNumber= 0; vertexNumber < gl_in.length(); ++vertexNumber)
	{
	    vec3 P = gl_in[vertexNumber].gl_Position.xyz;
	    vec3 N = vertex[vertexNumber].normal;
	    
	    gl_Position = MVP * vec4(P, 1.0);
	    EmitVertex();	    
	    gl_Position = MVP * vec4(P + (N *2.0f), 1.0);
	    EmitVertex();
	    
	    EndPrimitive();
	}
}