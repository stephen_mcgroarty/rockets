#version 410 core

layout(location = 0) out vec4 outColour;

uniform sampler2D textureID;
in vec2 UV;

void main()
{
	if (UV.x > 0.274 && UV.x < 0.275 
		&& UV.y > 0.66 && UV.y < 0.67)
	{
		outColour = vec4(1.0,0.0,0.0,1.0);
	} else {
	    outColour = texture(textureID,UV);
	}
}
