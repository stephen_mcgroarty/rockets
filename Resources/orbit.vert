#version 410 core
layout(location = 0) in vec4 model;
layout(location = 2) in vec3 normal;

out vec3 vertNormal;

void main()
{
	gl_Position= model;
    vertNormal = normal;
}