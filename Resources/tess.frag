#version 410 core
out vec4 outColour;
in vec4 teColour;

void main()
{
    outColour = teColour;
}