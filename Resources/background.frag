#version 410 core

out vec4 outColour;

const vec4 ground = vec4(0.1f,0.8f,0.1f,1.0f);

const vec4 sky = vec4(0.0f,0.0f,1.0f,1.0f);

const vec4 space = vec4(0.0f,0.0f,0.0f,1.0f);

uniform float drag = 0.0f;


void main()
{
	if( drag < 0.015f)
	{
	    outColour = mix(ground,sky,drag * (1/0.015f));
	}
	else
	{
	    outColour = mix(sky,space,drag);
	}
}