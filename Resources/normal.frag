#version 410 core

out vec4 outColour;

uniform vec4 colour = vec4(0.0f,0.0f,0.0f,1.0f);
in vec2 UV;
void main()
{
    outColour = colour;
}
